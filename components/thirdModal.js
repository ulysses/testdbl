import React, {Component} from 'react';
import {StyleSheet,Modal, Text, Button, View, Alert, Image} from 'react-native';
import { connect } from 'react-redux';


class ThirdModal extends Component {




  render() {
    return (
      <React.Fragment>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.props.reducer.third}
         >
              {/* <View style = {{height: '50%'}}>
            <Image style={{flex:1, height: undefined, width: undefined}}
        
        source={require('../assets/pic1.jpg')}
      />
</View> */}

<View style={{backgroundColor: 'black', alignSelf: 'stretch',width: '100%', height:'15%',
    justifyContent: 'flex-end'}}>

      <Text 
              style={{
                  position: 'absolute',
                  marginTop: '8%',
                  marginLeft: '3%',
                  color: 'white', 
                  fontSize: 35}}
                  >
                       Basic Rules
              </Text>

</View>

<View style={styles.container}>

<Text style = {styles.titleText}>We expect you to:</Text>


<Text style = {styles.listedText}>1. Use this app as it was intended.</Text>
<Text style = {styles.listedText}>2. Be polite and understanding towards other users.</Text>
<Text style = {styles.listedText}>3. ...</Text>

<Text style = {styles.titleText}>We ban for:</Text>

<Text style = {styles.listedText}>1. Scanning someone's QR code without the need.</Text>
<Text style = {styles.listedText}>2. Harrassing someone through the app.</Text>
<Text style = {styles.listedText}>3. Being rude and generally abusive.</Text>
<Text style = {styles.listedText}>4. Any other repeated misuse of the app</Text>

<Text style = {styles.titleText}>Disclamer:</Text>

<Text style = {styles.listedText}>Bans are determined on case by case basis using evidence sent by users and our code of behaviour.</Text>

    
<View style={styles.buttonContainer}>
<Button title="Accept" onPress={() => {
                  this.props.onModalThree()
                  
                }}/>
</View>

</View>


        </Modal>

        </React.Fragment>
    );
  }
}


const styles = StyleSheet.create({
    
  container: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  buttonContainer: {
      width: '40%',
      position: 'absolute',
  bottom:40
  },
  titleText: {
    color: 'red',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: '5%'
  },
  listedText:{
    marginTop:'3%',
    fontSize: 15,
    width: '80%',
  }
 
});

const mapStateToProps = (state) => {
  console.log(state)
  const { reducer } = state
  return { reducer }
};

const mapDispachToProps = dispatch => {
  return {
    onModalThree: () => dispatch({ type: "CLOSE_MODAL_3", value4: false, value5: true}),
   
  };
};

export default connect(mapStateToProps,
  mapDispachToProps
  )(ThirdModal)