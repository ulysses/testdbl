import React, {Component} from 'react';
import {StyleSheet,Modal, Text, Button, View, Alert, Image} from 'react-native';
import { connect } from 'react-redux';
class SecondModal extends Component {




  render() {
    return (
      <React.Fragment>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.props.reducer.second}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
            {/* <View style = {{height: '50%'}}>
            <Image style={{flex:1, height: undefined, width: undefined}}
        
        source={require('../assets/pic2.png')}
      />
</View> */}

<View style={{backgroundColor: 'black', alignSelf: 'stretch',width: '100%', height:'15%',
    justifyContent: 'flex-end'}}>

      <Text 
              style={{
                  position: 'absolute',
                  marginTop: '8%',
                  marginLeft: '3%',
                  color: 'white', 
                  fontSize: 35}}
                  >
                       Tutorial
              </Text>

</View>

<View style={styles.container}>

<Text style = {styles.titleText}>Quick Tutorial</Text>


<Text style = {styles.listedText}>1. Register and you will be prompted to set up you profile.</Text>
<Text style = {styles.listedText}>2. Create your personal QR code. Print out the QR code and stick it to the windshield of your car.</Text>
<Text style = {styles.listedText}>3. When someone you blocked scans your QR code, you will receive a notification. Same goes the other way.</Text>
<Text style = {styles.listedText}>4. To scan someone else, go to the Home tab, point your camera at thir QR code and press send.</Text>
<Text style = {styles.listedText}>5. You can track your requests, respond, and see responses in the Queue tab.</Text>
<Text style = {styles.listedText}>6. Every time you complete 100 successful leaves, you will receive a $10 gift card.</Text>
<Text style = {styles.listedText}>7. You can review this information again in the Info tab.</Text>
<View style={styles.buttonContainer}>
<Button title="Next" onPress={() => {
                  this.props.onModalTwo()
                  
                }}/>
</View>

</View>


        </Modal>

        </React.Fragment>
    );
  }
}


const styles = StyleSheet.create({
    
  container: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  buttonContainer: {
      width: '40%',
      position: 'absolute',
  bottom:40
  },
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: '5%'
  },
  listedText:{
    marginTop:'3%',
    fontSize: 15,
    width: '80%',
  }
 
});

const mapStateToProps = (state) => {
  console.log(state)
  const { reducer } = state
  return { reducer }
};

const mapDispachToProps = dispatch => {
  return {
    onModalTwo: () => dispatch({ type: "CLOSE_MODAL_2", value2: false,value3: true }),
   
  };
};

export default connect(mapStateToProps,
  mapDispachToProps
  )(SecondModal)