import React from 'react';
import {Modal, Text, TouchableHighlight,
    View, Alert, TextInput, Button, StyleSheet,
     Image, ProgressViewIOSComponent, Platform} from 'react-native';
import { Tile } from 'react-native-elements';

import Layout from '../../constants/Layout';

const BOTTOM_BAR_HEIGHT = !Platform.isPad ? 29 : 49 // found from https://stackoverflow.com/a/50318831/6141587

const Gift = ({ value, toDo, pic, title, caption }) => (
    <Tile
      value={value}
      onPress={toDo}
      imageSrc={pic}
      imageContainerStyle={styles.imageContainer}
      activeOpacity={0.9}
      title={title}
      titleStyle={styles.title}
      caption={caption}
      captionStyle={styles.caption}
      containerStyle={styles.container}
      featured
    />
    )

export default Gift;

const styles = StyleSheet.create({
    container: {
      //flex: 1,
      alignItems: 'center',
    },
    imageContainer: {
      width: Layout.window.width - 30,
      height: '100%',
      //height: Layout.window.height - BOTTOM_BAR_HEIGHT * 6,
      borderRadius: 20,
      overflow: 'hidden', // this does magic
    },
    title: {
      position: 'absolute',
      left: 10,
      bottom: 30,
    },
    caption: {
      position: 'absolute',
      left: 10,
      bottom: 10,
    },
  })

/**
 * <Image source={props.img} />
            <Text>{props.summary}</Text>
            <View style={{width:'40%'}}>
                <Button title="Redeem"/>
            </View>
 */