import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Modal, TouchableHighlight, Alert, Picker, Button } from 'react-native';
import PageTemplate from './pageTemplate';

import * as firebase from 'firebase';
import Firebase from '../../constants/config';

class messagingModal extends Component {

    state = {
        modalVisible: this.props.modalVisible,
        message: 'None'
    }

    uid = firebase.auth().currentUser.uid;

    pushMessage (data) {
        console.log('sending data', data);
        Firebase.database().ref('UsersList/' + this.uid + '/dblPark').set(
            data    
        );
    }

    render() {
        //console.log(this.state.modalVisible)
        return (
            <React.Fragment>
              <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
              }}>
                  <PageTemplate title={"Double Parking"} />

                <Text>
                    Choose from one of the messages below or enter your own. Select the time associated with each one.
                </Text>
                <View>
                <Picker
                    selectedValue={this.state.message}
                    style={{height: 50, width: 300}}
                    onValueChange={(itemValue) =>
                    this.setState({message: itemValue})
                }
                >
                    <Picker.Item label={`I will be back in ${10} minutes`} value="1" />
                    <Picker.Item label={`I will be back in ${20} minutes`} value="2" />
                </Picker>
                </View>
                <Button onPress={this.pushMessage(this.state.message)} title={'set messages'} />
              </Modal>
            </React.Fragment>
        );
    }
}

export default messagingModal;