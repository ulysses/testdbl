
import React from 'react';
import { StyleSheet, Text, SafeAreaView,ScrollView } from 'react-native';
import PageTemplate from './smallComponents/pageTemplate'


export default class Info extends React.Component {
    render() {

      const { navigate } = this.props.navigation;
      const navigateBack=()=>{
        navigate('Home')
      }

      return(
        <React.Fragment>
        <PageTemplate title={'Info'}/>

        <SafeAreaView style={styles.container} >
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}  >

        <Text style = {styles.titleText}>DriverCom is:</Text>

<Text style = {styles.text}>A communication faciliation app designed to make driver's life a little bit easier.</Text>
<Text style = {styles.text}>DriverCom 
is a tool to directly contact a person who double parked next to your car, blocked your car in some other way, or even blocked your driveway. 
Going in the other direction, DriverCom also lets you notify someone you blocked that you will be out in no time so they should not call the cops or 
other services to deal with you. DriverCom eliminates the hassle of worrying that your car is being towed away or issued a ticket, as well as stress of not knowing 
when your blocker is comming back. Best of all, it does with your privacy in mind. You are not leaving any of your personal information like name of phone number 
on your windshield - the app will do everything for you. See how in the next window.</Text>
<Text style = {styles.text}>DriverCom is designed and created by VeryCool Studio. You can visit and contact us at www.verycool-studio.com</Text>


<Text style = {styles.titleText}>Quick Tutorial</Text>


<Text style = {styles.text}>1. Register and you will be prompted to set up you profile.</Text>
<Text style = {styles.text}>2. Create your personal QR code. Print out the QR code and stick it to the windshield of your car.</Text>
<Text style = {styles.text}>3. When someone you blocked scans your QR code, you will receive a notification. Same goes the other way.</Text>
<Text style = {styles.text}>4. To scan someone else, go to the Home tab, point your camera at thir QR code and press send.</Text>
<Text style = {styles.text}>5. You can track your requests, respond, and see responses in the Queue tab.</Text>
<Text style = {styles.text}>6. Every time you complete 100 successful leaves, you will receive a $10 gift card.</Text>
<Text style = {styles.text}>7. You can review this information again in the Info tab.</Text>


<Text style = {styles.titleText}>We expect you to:</Text>


<Text style = {styles.text}>1. Use this app as it was intended.</Text>
<Text style = {styles.text}>2. Be polite and understanding towards other users.</Text>
<Text style = {styles.text}>3. ...</Text>

<Text style = {styles.titleText}>We ban for:</Text>

<Text style = {styles.text}>1. Scanning someone's QR code without the need.</Text>
<Text style = {styles.text}>2. Harrassing someone through the app.</Text>
<Text style = {styles.text}>3. Being rude and generally abusive.</Text>
<Text style = {styles.text}>4. Any other repeated misuse of the app</Text>

<Text style = {styles.titleText}>Disclamer:</Text>

<Text style = {styles.text}>Bans are determined on case by case basis using evidence sent by users and our code of behaviour.</Text>

        </ScrollView>
      </SafeAreaView>
      </React.Fragment>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1
      
    },
    titleText: {
      fontSize: 24,
      fontWeight: 'bold',
      marginTop: '5%'
    },
    scrollView: {
      
      marginHorizontal: 10,
    },
    text: {
      fontSize: 14,
      marginTop: '2%'
    },
  });