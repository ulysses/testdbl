import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'
import * as firebase from 'firebase';
import { connect } from 'react-redux';



class Loading extends React.Component {

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
          // user ? this.registerForPushNotificationsAsync(user) : '',
          // console.log(firebase.auth().currentUser.email)
          this.props.navigation.navigate(user ? 'Profile' : 'SignUp' )
          // Expo.Notifications.addListener(this.listen)
        })
      }

      // componentWillMount(){
      //   this.registerForPushNotificationsAsync();
      //   this.listener = 
      // }

      // componentWillUnmount(){
      //   this.listener && Expo.Notifications.removeListener(this.listen)
      // }

      // listen = () => {
      //   this.props.navigation.navigate('Queue')
      // }





      // registerForPushNotificationsAsync = async(user)=>{
      //   const { status: existingStatus } = await Permissions.getAsync(
      //     Permissions.NOTIFICATIONS
      //   );
      //   let finalStatus = existingStatus;
      
      //   // only ask if permissions have not already been determined, because
      //   // iOS won't necessarily prompt the user a second time.
      //   if (existingStatus !== 'granted') {
      //     // Android remote notification permissions are granted during the app
      //     // install, so this will only ask on iOS
      //     const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      //     finalStatus = status;
      //   }
      
      //   // Stop here if the user did not grant permissions
      //   if (finalStatus !== 'granted') {
      //     return;
      //   }
      
      //   // Get the token that uniquely identifies this device
      //   let token = await Notifications.getExpoPushTokenAsync();
    
      //   console.log(token)
    
      //   var updates = {}
      //   updates['/expoToken'] = token
    
      //   firebase.database().ref('contacts').child(user.uid).update(updates)
      // }

  render() {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})



const mapStateToProps = (state) => {
  console.log(state)
  const { reducer } = state
  return { reducer }
};

const mapDispachToProps = dispatch => {
  return {
    onModalOne: () => dispatch({ type: "CLOSE_MODAL_12", value: false}),
   
  };
};

export default connect(mapStateToProps,
  mapDispachToProps
  )(Loading)