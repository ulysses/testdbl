import React from 'react'
import { StyleSheet, Text, TextInput, View, Button } from 'react-native'

import ApiKeys from '../constants/ApiKeys';
import * as firebase from 'firebase';

export default class Login extends React.Component {
  state = { email: '', password: '', errorMessage: null }


  handleLogin = () => {
    const { email, password } = this.state
    
    firebase.auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => this.props.navigation.navigate('Scanner'))
      .catch(error => this.setState({ errorMessage: error.message }))
  }




  render() {
    return (
     
      <View style={styles.container}>
      <View style={{backgroundColor: 'black', alignSelf: 'stretch',width: '100%', height:'15%',
    justifyContent: 'flex-end'}}>

      <Text 
              style={{
                  position: 'absolute',
                  marginTop: '8%',
                  marginLeft: '3%',
                  color: 'white', 
                  fontSize: 35}}
                  >
                       Login
              </Text>

</View>

<View style={styles.InputContainer}>

       <Text style = {{marginBottom: '10%', fontSize: 25}}>Login</Text>
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}
        <TextInput
          style={styles.input}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          secureTextEntry
          style={styles.input}
          autoCapitalize="none"
          placeholder="Password"
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />



</View>

    


<View style={styles.buttonContainer1}>

        <Button title="Login" onPress={this.handleLogin} disabled = {this.state.email === '' || this.state.password === ''? true : false} />
</View>
       <View style={styles.buttonContainer}> 
        <Button
          title="Back"
          onPress={() => this.props.navigation.navigate('SignUp')}
        />
     
     </View>
     </View> 
     
    )
  }
}



const styles = StyleSheet.create({
    
  container: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  InputContainer: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  buttonContainer: {
      width: '55%',
      
  bottom:'10%'

  
  },
  buttonContainer1: {
    width: '55%',
    
    bottom:'15%'


},
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: '5%'
  },
  listedText:{
      marginTop:'3%',
    fontSize: 18
  },
  input: {
    width: 220,
    height: 44,
    padding: 10,
    borderWidth: 1.7,
    borderColor: 'black',
    marginBottom: 10,
  },


 
});