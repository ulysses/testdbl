import React from 'react';
import { StyleSheet, Text, View, StatusBar, TouchableOpacity, Image, FlatList, SectionList  } from 'react-native';

import PageTemplate from './smallComponents/pageTemplate';
import messages from '../assets/messages.png';
import rate from '../assets/smallRate.png';
import { Ionicons } from '@expo/vector-icons';


    export default class Queue extends React.Component {

    render() {

      const { navigate } = this.props.navigation;
      const navigateBack=()=>{
        navigate('Home')
      }

      let chatBtn =
        <TouchableOpacity 
          onPress={()=>navigate('Chat')}
          style={{
              borderWidth:1,
              //borderColor:'rgba(0,0,0,0.2)',
              alignItems:'center',
              justifyContent:'center',
              width:35,
              height:35,
              backgroundColor:'#fff',
              borderRadius:50
            }}>
              <Ionicons  name="ios-chatbubbles" size={32} />
        </TouchableOpacity>

      let rateBtn =
        <TouchableOpacity 
          style={{
            borderWidth:1,
            //borderColor:'rgba(0,0,0,0.2)',
            alignItems:'center',
            justifyContent:'center',
            width:35,
            height:35,
            backgroundColor:'#fff',
            borderRadius:50
          }}>
            {/*<Image source={rate}/>*/}
            <Ionicons  name="ios-heart" size={32} />
        </TouchableOpacity>
      
      return(
       
       <React.Fragment>
     <PageTemplate title={'Queue'} navigate={navigateBack} />
      <View style={styles.container}>
        <SectionList
          sections={[
            {title: 'Requests for Others to Move',
             data: ['Request 143xd']
            },
            {title: 'Requests for you to Move',
             data: ['Request 156ru']
            },
          ]}
          renderItem={({item}) => <View style={{flexDirection: 'row'}}><Text style={styles.item}>{`${item} Time Passed: 3:34 `}</Text>{chatBtn}{rateBtn}</View>}
          renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
          keyExtractor={(item, index) => index}
        />
      </View>
 

       </React.Fragment>
      );
    }
  }


  const styles = StyleSheet.create({

    btn : {
      borderWidth:1,
      borderColor:'rgba(0,0,0,0.2)',
      alignItems:'center',
      justifyContent:'center',
      width:35,
      height:35,
      backgroundColor:'#fff',
      borderRadius:50,
      marginTop: 150
    },
    
    btnWrapper: {
      position: 'absolute',
      flexDirection: 'row',
      flex: 2,
      justifyContent:'space-between',
      padding:15 
    },
  
    sliderWrapper: {
      position: 'absolute',
      marginTop: '150%',
      flexDirection: 'row'
    },
  
    textBox: {
      position: 'absolute',
      flexDirection: 'row',
      flex: 1.5,
      marginLeft: '25%',
      marginTop: '30%', 
    },
  
    text: {
      position: "absolute",
      color: "white",
      textAlign: 'center',
      fontSize: 30
    },
    
    textInput: {
      position: 'absolute',
      flexDirection: 'row',
      flex: 1.5,
      marginLeft: '25%',
      marginTop: '150%', 
    },
  
    container: {
      flex: 1,
      paddingTop: 22
     },
     item: {
       padding: 10,
       fontSize: 18,
       height: 44,
     },
     sectionHeader: {
      paddingTop: 2,
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 2,
      fontSize: 14,
      fontWeight: 'bold',
      backgroundColor: 'rgba(247,247,247,1.0)',
    }
    })
  


  /**
   * 
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'grey'}}>
          <Text> Queue page </Text>
        </View>
   * 





          <View style={styles.btnWrapper}>
            <TouchableOpacity 
              style={{
                borderWidth:1,
                //borderColor:'rgba(0,0,0,0.2)',
                alignItems:'center',
                justifyContent:'center',
                width:35,
                height:35,
                backgroundColor:'#fff',
                borderRadius:50
              }}>
                <Image source={messages}/>
            </TouchableOpacity>
          </View>


          <View>
              <Text>Your Placed Requests</Text>
              <Text>Request 1 . . . time: 16:48 . . . time since sent: 4:32</Text>
          </View>

          <View><Text>Requests for you to Move </Text></View>            
            
            
            <Text> Queue page </Text>

           
          



   */