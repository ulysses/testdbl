import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight,
   View, Alert, TextInput, Button, StyleSheet} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import Firebase from '../constants/config';
import PageTemplate from './smallComponents/pageTemplate';

class generateQR extends Component {

    state = {
        id: null,
        showCode: false        
    }

    handleCode=()=>{
        this.setState({ showCode: true })
    //         firebase.database().ref('Users/').once('value', function (snapshot) {
    //     console.log(snapshot.val())
    // });
    console.log(this.props.reducer.token)
    }

    render() {
        
        const { navigate } = this.props.navigation;
        navigateBack=()=>{
          navigate('Profile')
        }

        return (
            <React.Fragment>
                <View style={{backgroundColor:'white',flex:1, padding:10}} />
                    
                    <View>
                        <Text>Generate your QR code by pressing on the button below. You can always regenerate your QRCode if need be.</Text>    
                    </View>

                    <View style={{backgroundColor:'white',flex:1, padding:10}} />

                    {/** Enter your information and generate your code*/}
                    
                    {
                        this.state.showCode ?
                        <View style={{margin:110}}>
                            <QRCode value={this.props.reducer.token} />   
                        </View> 
                        :<View style={{margin:110,width:"40%"}}>
                            <Button title={'Show my Code'} onPress={this.handleCode} /> 
                         </View>
                    }

                    <View style={{backgroundColor:'white',flex:1, padding:10}} />
              
                    
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
   
    const { reducer } = state
    return { reducer }
  };
  
//   const mapDispachToProps = dispatch => {
//     // return {
//     //   setToken: (x) => dispatch({ type: "CLOSE_MODAL_13", value: x})
     
//     // };
//   };
  
  export default connect(mapStateToProps
    )(generateQR)



/**
 *   <View>
                    <QRCode content='https://reactnative.com'/>
                </View>


                 <View style={{margin: 20}}>
                        <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1 }} secureTextEntry={true}  />
                    
                    </View>



                    
 */
