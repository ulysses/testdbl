import React from 'react';
import { StyleSheet, Text, View,Header,Button } from 'react-native';
import PageTemplate from './smallComponents/pageTemplate';
import { connect } from 'react-redux';
import * as firebase from 'firebase';

import Qreader from './qReader';

 class Home extends React.Component {

  render() {
    
    const { navigate } = this.props.navigation;
    return (
      <React.Fragment>
        <PageTemplate title={'Scanner'} />
        
        <Qreader navigate={navigate}/>
       
      </React.Fragment>
          
    );
  }
}


/**
 *   <View>
          <Button
            title="Scan"
              onPress={() =>
                navigate('Queue')
            }
          /> 
        </View>
 * 
 * 
 */

const mapStateToProps = (state) => {
  const { reducer } = state
  return { reducer }
};

export default connect(mapStateToProps)(Home)