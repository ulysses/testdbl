import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { List, ListItem } from 'react-native-elements'

import GenerateQR from './generateQR';
import PageTemplate from './smallComponents/pageTemplate';
import MessagingModal from './smallComponents/messagingModal';

import pic from '../assets/bigRate.png';


class messaging extends Component {

    state={
        modalVisible: false
    }

    

    render() {
        return (
            <React.Fragment>
                <PageTemplate title={'Messaging'} />
                <Text>MoveIt users are able to add more messages that will be tied to their QR codes. The more information you provide for other users, the more you make! Choose from some of the predefined messages in the categories below and win automatic points that reup each month! Click on the choices below and select the choice that best fits</Text>

                <ListItem
                    onPress={()=>this.setState({ modalVisible:true})}
                    title={`Double Parking Away`}
                    subtitle={`Leave a message to tell scanners when you will be back from double parking`}
                    rightElement={`500 points`}
                />
                
                {this.state.modalVisible && ( <MessagingModal 
                                              modalVisible={this.state.modalVisible}
                                              />)}

                <ListItem
                    onPress={()=>{navigate('Messaging')}}
                    title={`General Parking Away`}
                    subtitle={`Leave scanners a message to let them know when you will vacate your spot`}
                    rightElement={`1000 points`}
                />

                <ListItem
                    onPress={()=>{navigate('QR')}}
                    title={`Insurance Information`}
                    subtitle={`Leave scanners a message with contact information for your insurance provider should they hit your car`}
                    rightElement={`2000 points`}
                />

              
            
            </React.Fragment>
        );
    }
}


export default messaging;