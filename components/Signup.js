import React from 'react'
import { StyleSheet, Text, TextInput, View, Button } from 'react-native'

import { connect } from 'react-redux';
import ignoreWarnings from 'react-native-ignore-warnings';

import ApiKeys from '../constants/ApiKeys';
import * as firebase from 'firebase';

import FirstModal from './firstModal'
import SecondModal from './secondModal'
import ThirdModal from './thirdModal'

import Firebase from '../constants/config';


ignoreWarnings('Setting a timer');

 class SignUp extends React.Component {
    state = { email: '', password: '', fname:null, lname:null, phone:null, errorMessage: null, uid: null}

 
    handleSignUp = () => {
      firebase.auth()

      
          .createUserWithEmailAndPassword(this.state.email, this.state.password)
          .then((user)=>{
            //console.log('uid',user.user.uid)
            //this.setState({ uid: user.user.uid })
            this.addUser(user.user.uid, 
                         this.state.fname, 
                         this.state.lname, 
                         this.state.phone, 
                         this.state.email, 
                         5000,
                         123456); //fake token for now
          })
          .then(() => this.props.navigation.navigate('Profile'))
        //   .then((res) => firebase.database().ref('users/' + res.user.uid).set({
        //     email: res.user.email,
        //     uid: res.user.uid
        // }))
          .catch(error => this.setState({ errorMessage: error.message }))

          //insert new record into our realtime database
          //this.addUser(this.state.uid,this.state.email,1000); //users are given 1000 in rewards to start
          
      }

      // when a user signs up they will have a record added to the user table in realtime database
      addUser = (id,fname,lname,phone,email,rewards,token) => {
        console.log(id)
        Firebase.database().ref('UsersList/' + id).set({
          id,
          fname,
          lname,
          phone,
          email,
          rewards,
          token
      }).then((data)=>{
          //success callback
          console.log('data ' , data)
      }).catch((error)=>{
          //error callback
          console.log('error ' , error)
      })
      }



render() {
    return (
<React.Fragment>
      <FirstModal/>
      <SecondModal/>
      <ThirdModal/>

      <View style={styles.container}>
        <View style={{backgroundColor: 'black', alignSelf: 'stretch',width: '100%', height:'15%',
    justifyContent: 'flex-end'}}>

        <Text 
                style={{
                    position: 'absolute',
                    marginTop: '8%',
                    marginLeft: '3%',
                    color: 'white', 
                    fontSize: 35}}
                    >
                         Registration
                </Text>

</View>

<View style={styles.InputContainer}>

        <Text style = {{marginBottom: '10%', fontSize: 25}}>Sign Up</Text>
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}
          <TextInput
          placeholder="First Name"
          autoCapitalize="none"
          style={styles.input}
          onChangeText={fname => this.setState({ fname })}
          value={this.state.fname}
        />
        <TextInput
          placeholder="Last Name"
          autoCapitalize="none"
          style={styles.input}
          onChangeText={lname => this.setState({ lname })}
          value={this.state.lname}
        />
        <TextInput
          placeholder="Phone Number"
          autoCapitalize="none"
          style={styles.input}
          onChangeText={phone => this.setState({ phone })}
          value={this.state.phone}
        />
        <TextInput
          placeholder="Email"
          autoCapitalize="none"
          style={styles.input}
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          secureTextEntry
          placeholder="Password"
          autoCapitalize="none"
          style={styles.input}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />
</View>

    
<View style={styles.buttonContainer1}>

        <Button title="Sign Up" onPress={this.handleSignUp} disabled = {this.state.email === '' || this.state.password === ''? true : false} />
</View>
       <View style={styles.buttonContainer}> 
        <Button
          title="Already Registered?"
          onPress={() => this.props.navigation.navigate('Login')}
        />
     
     </View>
     </View>
     </React.Fragment>
    )
  }
}


export default SignUp;



const styles = StyleSheet.create({
    
  container: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  InputContainer: {
      
    textAlign: 'center', // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e6eded'
  },
  buttonContainer: {
      width: '55%',
      
  bottom:'10%'

  
  },
  buttonContainer1: {
    width: '55%',
    
    bottom:'15%'


},
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: '5%'
  },
  listedText:{
      marginTop:'3%',
    fontSize: 18
  },
  input: {
    width: 220,
    height: 44,
    padding: 10,
    borderWidth: 1.7,
    borderColor: 'black',
    marginBottom: 10,
  },


 
});














/**



   */