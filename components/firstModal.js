import React, {Component} from 'react';
import {StyleSheet,Modal, Text, Button, View, Alert, Image,SafeAreaView,ScrollView} from 'react-native';
import { connect } from 'react-redux';

class FirstModal extends Component {


    setModalVisible(visible) {
        this.setState({modalVisible: visible});
      }

  render() {
      
    return (
        <React.Fragment>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.props.reducer.first}
          >
              {/* <View style = {{height: '50%'}}>
            
            <Image style={{flex:1, height: undefined, width: undefined}}
        
        source={require('../assets/pic3.jpg')}
      />
</View> */}
      <View style={{backgroundColor: 'black', alignSelf: 'stretch',width: '100%', height:'15%',
    justifyContent: 'flex-end'}}>

      <Text 
              style={{
                  position: 'absolute',
                  marginTop: '8%',
                  marginLeft: '3%',
                  color: 'white', 
                  fontSize: 35}}
                  >
                       Welcome
              </Text>

</View>

<View style={styles.container}>

<Text style = {styles.titleText}>DriverCom is:</Text>

<Text style = {styles.listedText}>A communication faciliation app designed to make driver's life a little bit easier.</Text>
<Text style = {styles.listedText}>DriverCom 
is a tool to directly contact a person who double parked next to your car, blocked your car in some other way, or even blocked your driveway. 
Going in the other direction, DriverCom also lets you notify someone you blocked that you will be out in no time so they should not call the cops or 
other services to deal with you. DriverCom eliminates the hassle of worrying that your car is being towed away or issued a ticket, as well as stress of not knowing 
when your blocker is comming back. Best of all, it does with your privacy in mind. You are not leaving any of your personal information like name of phone number 
on your windshield - the app will do everything for you. See how in the next window.</Text>
<Text style = {styles.listedText}>DriverCom is designed and created by VeryCool Studio. You can visit and contact us at www.verycool-studio.com</Text>

    
<View style={styles.buttonContainer}>
 <Button title="Next" onPress={() => {
                  this.props.onModalOne()
                  
                }}/>
</View>

</View>


        </Modal>

        </React.Fragment>
    );
  }
}


const styles = StyleSheet.create({
    
  container: {
      
     // <-- the magic
     
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: '#e6eded'
   
  },
  buttonContainer: {
      width: '40%',
      position: 'absolute',
  bottom:40
  },
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: '5%'
  },
  listedText:{
      marginTop:'3%',
    fontSize: 15,
    width: '80%',
    textAlign: 'center'
  }
 
});

const mapStateToProps = (state) => {
    console.log(state)
    const { reducer } = state
    return { reducer }
  };

  const mapDispachToProps = dispatch => {
    return {
      onModalOne: () => dispatch({ type: "CLOSE_MODAL_1", value: false, value1: true }),
     
    };
  };

  export default connect(mapStateToProps,
    mapDispachToProps
    )(FirstModal)

