//https://stackoverflow.com/questions/44603362/setting-a-timer-for-a-long-period-of-time-i-e-multiple-minutes
//yellow box issue we can ignore

import React, { Component } from 'react';
import { StyleSheet, Text, View, Animated, ScrollView, Alert, Dimensions } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { List, ListItem } from 'react-native-elements';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { connect } from 'react-redux';
import * as moment from "moment";

import GenerateQR from './generateQR';
import Gift from './smallComponents/gift';
import Amazon from '../assets/amazon.png';
import Starbux from '../assets/starbux.png';

//import GiftCards from '../constants/rewardsConfig';
import * as gifts from '../constants/rewardsConfig';

//initalize firebae db 
import Firebase from '../constants/config';
import * as firebase from 'firebase';
import ignoreWarnings from 'react-native-ignore-warnings';

//let db = Firebase.database();
//let rewardsRef = db.ref('/rewards');

ignoreWarnings('Setting a timer');
class rewards extends Component {

    state = {
        goal:50000,
        rewards: null,
        progress:null,
        progressWithOnComplete: 0,
        progressCustomized: 0,
        fname: null
        //uid:'znqiuJIFH7QYlFDyWd1kLIHnBkG3'
    };

    uid = this.props.reducer.data.uid;

    readUserData() {
        Firebase.database().ref('UsersList/').once('value', snapshot => {
        //console.log(snapshot.val());    
        let data = snapshot.val()[this.uid]
            //console.log('our data: ',data);
            this.setState({ rewards: data.rewards, fname: data.fname });
            this.calculateProgress(data.rewards);
        })
        
    }

    calculateProgress (data) {
        let progress =  Math.round(data/this.state.goal * 100);
        this.setState({ progress: progress });
        console.log(progress);
    }


    alterRewards (value) {
        //console.log('received data ', value);
        //console.log(this.state.rewards)
        let data = this.state.rewards - value;
        //console.log('ready to send', data)        
        this.pushReward(data);
    }

    pushReward (data) {
        console.log('sending data', data, 'to', this.uid);
        Firebase.database().ref('UsersList/' + this.uid + '/rewards').set(
            data    
        );
        this.logReward(this.uid,'1',data);
        this.updateRewards();
    }

    logReward (id,rewards,type) {
        console.log(id)
        let time =new Date(); //moment().format("YYYY-MM-DD HH:mm:00");
        Firebase.database().ref('Rewards/' + id).set({
          type,
          rewards,
          time
      }).then((data)=>{
          //success callback
          console.log('data ' , data)
      }).catch((error)=>{
          //error callback
          console.log('error ' , error)
      })
      }

    updateRewards () {
        this.readUserData();
    }

    chooseReward (data) {
        //console.log('pressed-------------------', data)

        Alert.alert(
             this.state.fname + ` are you sure you want to buy this reward, it will cost you 600 points?`,
            '',
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
              {text: 'OK', onPress:()=>this.alterRewards(data)},
            ]
            )
    }


    componentDidMount () {
        this.readUserData();
    }

    increase = (key, value) => {
        this.setState({
          [key]: this.state[key] + value,
        });
    }

    
    render() {

        //set up loop for component
        

        //console.log(firebase.auth().currentUser.email);
        //console.log(firebase.auth().currentUser.uid);
        //console.log('my obj: ',gifts.giftCards[0])

        
        let cards = null;
        if(this.state.rewards) {
            cards = (
                <View>
                    {
                        gifts.giftCards.map( (card) => {
                            return <Gift
                            value={card.value}
                            toDo={() => this.chooseReward(card.value)}
                            pic={card.pic}
                            title={card.id}
                            caption={''}
                        />
                        })
                    }
                </View>
            )
        }

        
        

       const barWidth = Dimensions.get('screen').width - 130;

        return (
            <ScrollView style={{marginTop: 30}}>
                <View style={styles.container}>
                <Text>{this.state.fname && `Welcome to your Rewards ${this.state.fname}!`}</Text>
                <Text>{`Your Points: ${this.state.rewards ? this.state.rewards.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0}`}</Text>

                {/**PROGRESS BAR */}
                
                <View style={styles.containerBar}>
                    <View style={{alignItems: 'center'}}>
                        <Text>
                            Progress to Next Reward
                        </Text>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <ProgressBarAnimated
                            width={barWidth}
                            value={this.state.progress}
                            backgroundColorOnComplete="#6CC644"
                        />
                    </View>
                <Text>
                    {`${this.state.progress}% Progress`}
                </Text>

                </View>

                {/**GIFT CARD OPTIONS */}
                <Text>MoveIt Cash Offers</Text>
                <Text>Your MoveIt points can be used to redeem the following deals.</Text>
                {cards}
                </View>
             </ScrollView>    
              
            
        );
    }
}

const mapStateToProps = (state) => {
    console.log(state)
    const { reducer } = state
    return { reducer }
  };

  const mapDispachToProps = dispatch => {
    return {
      pullUser: () => dispatch({ type: "GET_USER_DATA", value: false}),
    };
  };
  
  //export default connect(mapStateToProps,
  //  mapDispachToProps
  //  )(Loading)
  

export default rewards;

const styles = StyleSheet.create({
    container: {
      //backgroundColor: 'yellow',
      //flex: 1,
      //flexWrap: 'wrap',
      alignItems: 'center',
      justifyContent:'space-between',
      padding:15 
    },
    containerBar: {
        //flex: 1,
        //flexDirection: 'Column',
        //justifyContent: 'center',
        //alignItems: 'center',
        //paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
        //padding: 8,
        //width: '90%'
          alignSelf: 'center',
          padding: 12
      },
      progressBar: {
        flexDirection: 'row',
        height: 20,
        width: '100%',
        backgroundColor: 'white',
        borderColor: '#000',
        borderWidth: 2,
        borderRadius: 5
      },
      label: {
        color: '#999',
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 10,
      },
  })


  /**
   * 
   * let cards = null;
        if(this.state.rewards) {
            cards = (
                <View>
                    {
                        gifts.giftCards.map( (card, index) => {
                            return <Gift
                            value={gifts.giftCards['amz']['1']}
                            toDo={() => this.chooseReward(gifts.giftCards['amz']['1'])}
                            pic={gifts.cardPics.amz}
                            title={'$10'}
                            caption={'here is a gift card'}
                        />
                        })
                    }
                </View>
            )
        }
   * 
   */