import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight, TextInput } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { List, ListItem } from 'react-native-elements'

import GenerateQR from './generateQR';
import PageTemplate from './smallComponents/pageTemplate';
import pic from '../assets/bigRate.png'


class editAccount extends Component {
    render() {
        return (
            <React.Fragment>
                <View style={{marginLeft: '10%'}}>
                <TextInput
                    value='' 
                    placeholder={'First Name'}
                    style={styles.input}
                />
                <TextInput
                    value=''  
                    placeholder={'Last Name'}
                    style={styles.input}
                />
                <TextInput
                    value=''  
                    placeholder={'Email'}
                    style={styles.input}
                />
                </View>
            </React.Fragment>   
        );
    }
}

export default editAccount;


const styles = StyleSheet.create({
    
    container: {
        
      textAlign: 'center', // <-- the magic
       
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      //justifyContent: 'center',
      backgroundColor: '#e6eded'
    },
    InputContainer: {
        
      textAlign: 'center', // <-- the magic
       
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#e6eded'
    },
    buttonContainer: {
        width: '40%',
        position: 'absolute',
    bottom:40
    },
    titleText: {
      fontSize: 25,
      fontWeight: 'bold',
      marginTop: '5%'
    },
    listedText:{
        marginTop:'3%',
      fontSize: 18
    },
    input: {
      width: 200,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginBottom: 10,
    },
  
   
  });