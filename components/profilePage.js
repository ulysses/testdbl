
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { List, ListItem } from 'react-native-elements'

import GenerateQR from './generateQR';
import PageTemplate from './smallComponents/pageTemplate';
import pic from '../assets/bigRate.png';
import * as firebase from 'firebase';

import Firebase from '../constants/config';

import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as Expo from 'expo'
import { connect } from 'react-redux';

class Profile extends React.Component {

state = {token: '', fname: null, lname: null, phone: null, email: null, data: null}

uid = firebase.auth().currentUser.uid;


  componentDidMount() {

    if (firebase.auth().currentUser) {
      userId = firebase.auth().currentUser.uid;
      if (userId) {
          firebase.database().ref('users/' + userId).set({
              email: firebase.auth().currentUser.email,
              
              uid: userId,
             
          })
      }
    }
   
      this.registerForPushNotificationsAsync()

      Expo.Notifications.addListener(this.listen)

      this.readUserData();
    
  }

  // componentWillMount(){
  //   this.registerForPushNotificationsAsync();
  //   this.listener = 
  // }

  // componentWillUnmount(){
  //   this.listener && Expo.Notifications.removeListener(this.listen)
  // }

  listen = () => {
    this.props.navigation.navigate('Queue')
  }


  registerForPushNotificationsAsync = async()=>{
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
  
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
  
    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
  
    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();

    console.log(token)
    // firebase.database().ref('users/' + userId).push({token: token })
    var updates = {}
    updates['/expoToken'] = token

    firebase.database().ref('users/' + userId).update(updates)


    this.setState({
      token: token,
    });

 

  }


  // sendPushNotification(token = this.state.token, title = 'test', body = 'test') {
  //   return fetch('https://exp.host/--/api/v2/push/send', {
  //     body: JSON.stringify({
  //       to: token,
  //       title: title,
  //       body: body,
  //       data: { message: `${title} - ${body}` },
  //     }),
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     method: 'POST',
  //   });
  // }

    signOut = () => {
      firebase.auth().signOut()
        .catch(error => console.log(error))
    }

    handleSignout = () => {
      console.log('signing out')
      Alert.alert(
        'Are you sure you want to sign out?',
        'Confirm Signing Out',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
          {text: 'OK', onPress: this.signOut},
        ]
        )
    
    }

    readUserData() {
      Firebase.database().ref('UsersList/').once('value', snapshot => {
      //console.log(snapshot.val());    
      let data = snapshot.val()[this.uid]

          //console.log('our data: ',data);
          this.setState({ fname: data.fname,
                          lname: data.lname,
                          phone: data.phone,
                          email: data.email,
                          data: data});
          //this.calculateProgress(data);
          this.state.data && this.props.setData(this.state.data);

      })
      
    }


    render() {
      const { navigate } = this.props.navigation;
      navigateBack=()=>{
        navigate('Queue')
      }

      this.state.data && console.log('from state ',this.state.data)
      this.props.reducer.data && console.log('from store', this.props.reducer.data)

      return(
        <React.Fragment>
         
         <PageTemplate title={'Profile'} />
          
          {/*<Image source={pic} />*/}
          {/**  Frst Section */}

          <View style={{ 
                  //backgroundColor:'blue'
                  }}>
                  <Text style={styles.section}>Account Information</Text>
                  
          </View>
          <TouchableOpacity
                        onPress={()=>{navigate('EditAccount')}}
                        style={{position: 'absolute',
                        marginTop:'33%',
                        flex:1,
                        flexDirection: 'row',
                        flexWrap: 'wrap'}}>
          
              <View style={{
                            marginLeft:'5%',
                            flex:1,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            justifyContent: 'flex-start',
                            //backgroundColor:'yellow',
                            alignItems: 'center',
                            //borderRadius:50,
                            //height:'60%'
                            }} >
                <Ionicons style={{marginLeft:'20%'}}  name="ios-person" size={72} />
              </View>
              
              <View style={{paddingTop:50,
                            marginRight:'40%',
                            
                            flex:2,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            //backgroundColor: 'red'
                          }}
                            >
                
                <Text>{this.props.reducer.data && this.props.reducer.data.fname + ' ' + this.props.reducer.data.lname }</Text>
                <Text>{this.props.reducer.data && this.props.reducer.data.phone}</Text>
                <Text>{this.props.reducer.data && this.props.reducer.data.email}</Text>
                <TouchableOpacity onPress={() => this.sendPushNotification()}>
          <Text>Send me a notification!</Text>
        </TouchableOpacity>
              </View>
            
         
          </TouchableOpacity>

          {/**add line section */}
          
          <View style={{
                    //backgroundColor:'yellow', 
                    marginTop:'33%'
                    }}>
                  <Text style={styles.section}>Settings</Text>
            <ListItem
              onPress={()=>{this.props.setToken(this.state.token), navigate('QR')}}
              title={`My Qr Code`}
              subtitle={`Print out your QR code to receive messages`}
              leftElement={<Ionicons  name="ios-qr-scanner" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            />

            <ListItem
              onPress={()=>{navigate('Messaging')}}
              title={`My Messages`}
              subtitle={`Edit your messages and receive points for more the more information you add`}
              leftElement={<Ionicons  name="ios-qr-scanner" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            />

            <ListItem
              onPress={()=>{navigate('Rewards')}}
              title={`My Rewards`}
              subtitle={`Check the status of your points to redeem for rewards`}
              leftElement={<Ionicons  name="ios-gift" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            />

            <ListItem
              onPress={()=>{navigate('Privacy')}}
              title={`Privacy`}
              subtitle={`Adjust your privacy settings`}
              leftElement={<Ionicons  name="ios-settings" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            />
            <ListItem
              onPress={this.handleSignout}
              title={`Sign Out`}
              subtitle={`Adjust your privacy settings`}
              leftElement={<Ionicons  name="ios-close-circle" size={32} />}
              rightElement={<Ionicons  name="ios-arrow-forward" size={32} />}
            />
            

          </View>
             


          
              

          {/** <GenerateQR /> */}
        </React.Fragment>
      );
    }
  }

  const styles = StyleSheet.create({

    option : {
      //position: 'absolute',
      marginLeft:'5%',
      alignSelf: 'stretch',
      width: '100%',
      height: '15%',
      //flexWrap: 'wrap',
      justifyContent:'space-between',
      //padding:20 
    },
    section : {
      fontSize:20,
      marginLeft:'3%',
      marginTop: '5%'
    }

  })

  const mapStateToProps = (state) => {
    console.log(state)
    const { reducer } = state
    return { reducer }
  };
  
  const mapDispachToProps = dispatch => {
    return {
      setToken: (x) => dispatch({ type: "CLOSE_MODAL_13", value: x}),
      setData: (y) => dispatch({ type: "GET_USER_DATA", value: y})
     
    };
  };
  
  export default connect(mapStateToProps,
    mapDispachToProps
    )(Profile)


  /**
   * 
   * 
   * 

                  <View style={styles.option}>
                    <Ionicons  name="ios-gift" size={32} />
                    <TouchableHighlight>
                      <Text>Rewards</Text>
                    </TouchableHighlight>
                  </View>



                  
   * 
   */