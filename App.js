import React from 'react';
import { StyleSheet, Text, View,StatusBar,Header, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
// import { Ionicons } from '@expo/vector-icons';



import ApiKeys from './constants/ApiKeys';
import * as firebase from 'firebase';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

import ourReducer from './store/reducer';
import Icon from "react-native-vector-icons/FontAwesome";

//main tabs
import Scanner from './components/mainPage';
import Queue from './components/queuePage';
import Info from './components/infoPage';
import Profile from './components/profilePage';
import Login from './components/Login'
import SignUp from './components/Signup'
import Load from './components/Loading'

//Profile Page Imports
import GenerateQR from './components/generateQR';
import EditAccount from './components/editAccount';
import Rewards from './components/rewards';
import Privacy from './components/privacy';
import Messaging from './components/messaging';

//Queue PAge Imports
import Chat from './components/chat';



import Firebase from './constants/config'
const store = createStore(ourReducer);
//firebase.initializeApp(ApiKeys.FirebaseConfig)
export default class App extends React.Component {

  // componentDidMount () {
  //   if (!firebase.apps.length) { firebase.initializeApp(ApiKeys.FirebaseConfig); }
  // }





  render() {


    
    return (
      <Provider store={ store }>


        <AppContainer />

      
        </Provider>
    );
  }
}



const bottomTabNavigator = createBottomTabNavigator(
  {
    Scanner: {
      screen: Scanner,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="qrcode" size={25} color={tintColor} />
        )
      }
    },
    Queue: {
      screen: Queue,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="comments" size={25} color={tintColor} />
        )
      }
    },
    Info: {
      screen: Info,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="search" size={25} color={tintColor} />
        )
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="user" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    initialRouteName: 'Profile',
    tabBarOptions: {
      activeTintColor: '#eb6e3d'
    }
  }
);

 //Navigate profiles
 const ProfileNavigator = createStackNavigator({
   Profile: { screen: Profile},
   QR: { screen: GenerateQR, navigationOptions },
   Messaging: { screen: Messaging, navigationOptions },
   Rewards: { screen: Rewards, navigationOptions },
   Privacy: { screen: Privacy, navigationOptions },
   EditAccount: { screen: EditAccount, navigationOptions }
 });

 //function nullifies headers when navigating back
ProfileNavigator.navigationOptions = () => {
  const navigationOptions = {
    header: null,
    headerMode: 'none',
  };
  return navigationOptions;
};

 //Navigate Queue
 const QueueNavigator = createStackNavigator({
  Queue: { screen: Queue},
  Chat: { screen: Chat, navigationOptions },
  
});


const RootSwitch = createSwitchNavigator({ Load, SignUp, Login, bottomTabNavigator,ProfileNavigator,QueueNavigator});

const AppContainer = createAppContainer(RootSwitch);



// //Stack Navigators----------------------------------------------------------------------
// //Each one of these allows us to navigate within the tabs created on the top

//Create a navigation options that will present a header for navigation use
 const navigationOptions = ({ navigation }) => {
   return { 
   headerTitle: <Text style={{fontSize:30, color:"white"}}>PlaceHolder</Text>,
     headerStyle: {
       height: '45%',
       backgroundColor: 'black'
     },
     headerTintColor: 'white',
     headerLeft: (
       <TouchableWithoutFeedback 
           style={{margin:20}}
           onPress={() => navigation.goBack()} >
           <Ionicons style={{margin:15}}  name="ios-arrow-dropleft" size={32} color="white" />
       </TouchableWithoutFeedback>
     )
   }
 } 

// //Navigate the comment modal
// const QRNavigator = createStackNavigator({
//   Home: { screen: Home},
//   //CommentModal: { screen: CommentModal, navigationOptions }
// });

// //Navigate profiles
// const ProfileNavigator = createStackNavigator({
//   Profile: { screen: Profile},
//   QR: { screen: GenerateQR, navigationOptions },
//   Rewards: { screen: Rewards, navigationOptions },
//   Privacy: { screen: Privacy, navigationOptions },
//   EditAccount: { screen: EditAccount, navigationOptions }
// });

// 

// //couch all navigation across app
// const AppNavigator = createSwitchNavigator({
//   tabs: bottomTabNavigator,
//   profile: ProfileNavigator
// })

// const AppContainer = createAppContainer(AppNavigator);