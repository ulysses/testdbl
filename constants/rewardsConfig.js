import Amazon from '../assets/amazon.png';

let giftCards = 
    [
        {id: 5, name: 'amz', value: 300, pic: Amazon},
        {id: 10, name: 'amz', value: 600, pic: Amazon},
        {id: 15, name: 'amz', value: 900, pic: Amazon}
    ];

let cardPics={
    'amz': Amazon
}

export {
    cardPics,
    giftCards
}
